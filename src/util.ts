import fs from 'fs';

export function checkEnv(name: string) {
  if (process.env[name] == null) {
    throw new Error(`${name} is missing from .env`);
  }
}

export function getConfigValue(envPropName: string | string[], rawValuePrefix = '-----BEGIN'): string {
  const res = searchConfigValue(envPropName, rawValuePrefix);
  if (res == null) {
    throw new Error(`${Array.isArray(envPropName) ? 'One of ' + envPropName.join(', ') : envPropName} is missing from .env or ./*`);
  }
  return res;
}

function searchConfigValue(envPropName: string | string[], rawValuePrefix: string): string | undefined {
  if (Array.isArray(envPropName)) {
    return envPropName.map(p => searchConfigValue(p, rawValuePrefix)).find(p => p);
  }
  const configValue = process.env[envPropName];
  if (rawValuePrefix != null && configValue?.startsWith(rawValuePrefix)) {
    return configValue;
  }
  if (fs.existsSync(configValue ?? envPropName)) {
    return fs.readFileSync(configValue ?? envPropName, 'utf-8');
  }
}
import { Kafka, KafkaConfig, EachMessagePayload, Producer, Consumer } from 'kafkajs';
import { ConnectionOptions } from 'tls';
import { checkEnv, getConfigValue } from './util';

require('dotenv').config();
checkEnv('KAFKA_BROKERS');

const sslConfig: ConnectionOptions = {
  ca: [getConfigValue(['SSL_CA', 'SSL_CA_LOCATION', 'ca.pem'])],
  cert: getConfigValue(['SSL_CERTIFICATE', 'SSL_CERTIFICATE_LOCATION', 'service.cert']),
  key: getConfigValue(['SSL_KEY', 'SSL_KEY_LOCATION', 'service.key']),
}

class KafkaService {
  kafka!: Kafka;
  producer!: Producer;
  consumer!: Consumer;

  async connect() {
    const kafkaConfig: KafkaConfig = {
      brokers: process.env.KAFKA_BROKERS!.split(','),
      clientId: process.env.KAFKA_CLIENT_ID,
      ssl: sslConfig
    };
    this.kafka = new Kafka(kafkaConfig);
    this.producer = this.kafka.producer();
    this.consumer = this.kafka.consumer({ groupId: process.env.KAFKA_GROUP_ID || 'credentialID' });
    await this.producer.connect();
    await this.consumer.connect();
  }

  async disconnect() {
    this.producer.disconnect();
    this.consumer.disconnect();
  }

  async sendMessage(topic: string, message: string | Buffer) {
    try {
      await this.producer.send({
        topic,
        messages: [{ value: message }],
      });
      console.log('sent to ' + topic, message.toString());
    } catch (error) {
      console.error('Error sending message:', error);
    }
  }
  
  async consumeMessages(topic: string) {
    await this.consumer.subscribe({ topic });
    await this.consumer.run({
      eachMessage: async ({ message }: EachMessagePayload) => {
        console.log('received from ' + topic, message.value?.toString());
      },
    });
  }

}

(async function main() {
  const ks = new KafkaService();
  ks.connect();
  await ks.consumeMessages('credentialID');
  await ks.sendMessage('credentialID', 'Hello world');
})();

